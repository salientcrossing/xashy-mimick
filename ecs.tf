resource "aws_ecs_cluster" "zproj-cluster" {
  name = "my-cluster"
}

resource "aws_ecs_cluster_capacity_providers" "zprojpro" {
  cluster_name = aws_ecs_cluster.zproj-cluster.name

  capacity_providers = ["FARGATE"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}
