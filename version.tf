terraform {
  required_version = "~>1.2.6"

  required_providers {
    aws = {
        version = "~> 4.7.0"
        source = "hashicorp/aws"
    }
  }
}